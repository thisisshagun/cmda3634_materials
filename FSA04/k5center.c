#include <stdio.h>
#include <stdlib.h>
#include <float.h>
#include <math.h>
#include <time.h>

// structure for a 2d vector
typedef struct vec2_s {
    double x,y;
} vec2;

// functions for a 2d vector
double vec2_dist_sq (vec2 u, vec2 v) {
    return (u.x-v.x)*(u.x-v.x) + (u.y-v.y)*(u.y-v.y);
}

// calc the cost squared for centers with indices c[0], c[1], c[2], c[3], c[4]
double calc_cost_sq (vec2 data[], int n, int c[]) {
    double cost_sq = 0;
    for (int i=0;i<n;i++) {
        double min_dist_sq = vec2_dist_sq(data[i],data[c[0]]);
        for (int j=1;j<5;j++) {
            double dist_sq = vec2_dist_sq(data[i],data[c[j]]);
            if (dist_sq < min_dist_sq) {
                min_dist_sq = dist_sq;
            }
        }
        if (min_dist_sq > cost_sq) {
            cost_sq = min_dist_sq;
        }
    }
    return cost_sq;
}

int main () {

    // read the number of points
    int n;
    if (scanf("%d",&n) != 1) {
        printf ("error reading the number of points\n");
        exit(1);
    }

    // read the data matrix
    vec2 data[n];
    for (int i=0;i<n;i++) {
        if (scanf("%lf %lf",&(data[i].x),&(data[i].y)) != 2) {
            printf ("error reading the data matrix\n");
            exit(1);
        }
    }

    // start the timer
    clock_t start = clock();

    // compute the minimal cost and an optimal solution
    double min_cost_sq = DBL_MAX;
    int optimal_centers[5];
    int tuples_checked = 0;
    int c[5];
    for (c[0]=0; c[0]<n-4; c[0]++) {
        for (c[1]=c[0]+1; c[1]<n-3; c[1]++) {
            for (c[2]=c[1]+1; c[2]<n-2; c[2]++) {
                for (c[3]=c[2]+1; c[3]<n-1; c[3]++) {
                    for (c[4]=c[3]+1; c[4]<n; c[4]++) {		    
                        tuples_checked += 1;
                        double cost_sq = calc_cost_sq(data,n,c);
                        if (cost_sq < min_cost_sq) {
                            min_cost_sq = cost_sq;
                            for (int i=0;i<5;i++) {
                                optimal_centers[i] = c[i];
                            }
                        }
                    }
                }
            }
        }
    }

    // stop the timer
    clock_t stop = clock();
    double elapsed = (double)(stop-start)/CLOCKS_PER_SEC;

    // print the results
    printf ("number of points = %d\n",n);
    printf ("5-tuples checked = %d\n",tuples_checked);
    printf ("elapsed time = %.2f seconds\n",elapsed);
    printf ("5-tuples checked per second = %.0f\n",tuples_checked/elapsed);
    printf ("minimal cost = %.2f\n",sqrt(min_cost_sq));
    printf ("optimal centers : %d %d %d %d %d\n",optimal_centers[0],
            optimal_centers[1],optimal_centers[2],optimal_centers[3],
            optimal_centers[4]);

    // success (optional in C99)
    return 0;
}
