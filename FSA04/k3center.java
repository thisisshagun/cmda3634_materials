import java.util.Scanner;

// class for a 2d vector
class vec2 {
    double x,y;

    public vec2 (double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double dist_sq (vec2 v) {
        return (this.x-v.x)*(this.x-v.x) + (this.y-v.y)*(this.y-v.y);
    }
};

class k3center {

    // calc the cost squared for centers with indices c[0], c[1], c[2]
    static double calc_cost_sq (vec2 data[], int c[]) {
        double cost_sq = 0;
        for (int i=0;i<data.length;i++) {	
            double min_dist_sq = data[i].dist_sq(data[c[0]]);	    
            for (int j=1;j<3;j++) {
                double dist_sq = data[i].dist_sq(data[c[j]]);
                if (dist_sq < min_dist_sq) {
                    min_dist_sq = dist_sq;
                }
            }
            if (min_dist_sq > cost_sq) {
                cost_sq = min_dist_sq;
            }
        }
        return cost_sq;
    }

    public static void main(String args[]) {

        // create a scanner for standard input                                  
        Scanner input = new Scanner(System.in);

        // read the number of points
        int n = input.nextInt();

        // read the data matrix
        vec2 data[] = new vec2[n];
        for (int i=0;i<n;i++) {
            double x = input.nextDouble();
            double y = input.nextDouble();
            data[i] = new vec2(x,y);
        }

        // start the timer
        long start = System.nanoTime();

        // compute the minimal cost and an optimal solution
        double min_cost_sq = Double.MAX_VALUE;
        int optimal_centers[] = new int[3];
        int tuples_checked = 0;
        int c[] = new int[3];
        for (c[0]=0; c[0]<n-2; c[0]++) {
            for (c[1]=c[0]+1; c[1]<n-1; c[1]++) {
                for (c[2]=c[1]+1; c[2]<n; c[2]++) {
                    tuples_checked += 1;
                    double cost_sq = calc_cost_sq (data,c);
                    if (cost_sq < min_cost_sq) {
                        min_cost_sq = cost_sq;
                        for (int i=0;i<3;i++) {
                            optimal_centers[i] = c[i];
                        }
                    }
                }
            }
        }

        // stop the timer
        long stop = System.nanoTime();
        double elapsed = (double)(stop-start)/1.0e9;

        // print the results
        System.out.printf ("number of points = %d\n",n);
        System.out.printf ("3-tuples checked = %d\n",tuples_checked);
        System.out.printf ("elapsed time = %.2f seconds\n",elapsed);
        System.out.printf ("3-tuples checked per second = %.0f\n",tuples_checked/elapsed);
        System.out.printf ("minimal cost = %.2f\n",Math.sqrt(min_cost_sq));
        System.out.printf ("optimal centers : %d %d %d\n",optimal_centers[0],
                optimal_centers[1],optimal_centers[2]);
    }
};
